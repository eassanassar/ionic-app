// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var db = null;
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova'])

.run(function($rootScope, $q, $ionicPlatform, $cordovaSQLite) {
	//$rootScope.ajaxCall = $q.defer();
	$rootScope.deferred = $q.defer();

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	window.plugins.sqlDB.remove("PM2016.db", 0, function(){console.log("removed success")},function(){console.log("removed error")});
//	 window.plugins.sqlDB.copy("natVal2015.db", 0 ,function() {
        db = $cordovaSQLite.openDB({ name: "PM2016.db"});
        var catTableQuery = "CREATE TABLE IF NOT EXISTS category (id integer primary key autoincrement, name text)";
        var subCatTableQuery = "CREATE TABLE IF NOT EXISTS sub_category (id integer primary key autoincrement, catid INTEGER REFERENCES category(id) ON UPDATE CASCADE, name text)";
        var dataTableQuery = "CREATE TABLE IF NOT EXISTS data (id integer primary key, catid INTEGER REFERENCES category(id) ON UPDATE CASCADE, subcatid INTEGER REFERENCES sub_category(id) ON UPDATE CASCADE,name text, date text, paid real, comment text )";
        createTable(catTableQuery,"CATEGORY");
        createTable(subCatTableQuery,"SUB CATEGORY");
        createTable(dataTableQuery,"DATA");

        function createTable(query , tableName){
          $cordovaSQLite.execute(db, query).then(function() {
            console.log("Create "+ tableName +" success");
            $rootScope.deferred.resolve();
          }, function (err) {
            console.error( "error building table "+ tableName+" Or Already Exist");
            console.error( err);
            //	return -1;
          });
        }
		 console.log("copied");

    //db = $cordovaSQLite.openDB("test.db");
	console.log(db);
    //$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS abb (id integer primary key, firstname text, lastname text)");
//	return deferred.promise;

  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-category.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.category-sub', {
      url: '/dash/:catId',
      views: {
        'tab-dash': {
          templateUrl: 'templates/category-sub.html',
          controller: 'SubCatCtrl'
        }
      }
    })
    .state('tab.sub-data', {
      url: '/dash/:catId/:subCatId',
      views: {
        'tab-dash': {
          templateUrl: 'templates/sub-data.html',
          controller: 'DataCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});

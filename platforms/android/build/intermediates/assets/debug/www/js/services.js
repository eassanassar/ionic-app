angular.module('starter.services', [])

.factory('Category', function($cordovaSQLite){
	return{
		all: function(){
			var query = "select * from category";
			console.log(query);
			var category = [];
			$cordovaSQLite.execute(db, query, []).then(function( res) {
				//var cat = {}
				console.log(res.rows);
				console.log(res.rows.item(0));
				for(var i=0 ;i < res.rows.length ; i++ ){
					category.push(res.rows.item(i));
				}
			}, function (err) {
				console.error( "all - Search Error" + err);
			//	return -1;
			});
			return category;

		},

		delete: function(id){
			var query = "DELETE FROM category where  id = ?";
			$cordovaSQLite.execute(db, query, [id]).then(function(res) {
				console.log("deleted : " + id + " " + res);
			 }, function (err) {
				console.error("Error Delete Category");
				console.error( err);
			});
		},

		update: function(id,name){
			var query = "UPDATE category set name = ? where  id = ?";
			$cordovaSQLite.execute(db, query, [name,id]).then(function(res) {
				console.log("deleted : " + id + " " + res);
			 }, function (err) {
				console.error("Error Delete Category");
				console.error( err);
			});
		},

		add: function(name) {
			var query = "INSERT INTO category(name) VALUES (?)";
				$cordovaSQLite.execute(db, query, [name]).then(function(res) {
				console.log("insertId: " + res);
			}, function (err) {
				console.error("Error");
				console.error( err);
			});
		}

	};
})

.factory('SubCategory', function($cordovaSQLite){
    return{
      all: function(catid){
        var query = "select * from sub_category where catid = ? ";
        var category = [];
        $cordovaSQLite.execute(db, query, [catid]).then(function( res) {
          //var cat = {}
          console.log(res.rows);
          console.log(res.rows.item(0));
          for(var i=0 ;i < res.rows.length ; i++ ){
            category.push(res.rows.item(i));
          }
        }, function (err) {
          console.error( "all - Search Error" + err);
          //	return -1;
        });
        return category;

      },

      delete: function(id){
        var query = "DELETE FROM sub_category where  id = ?";
        $cordovaSQLite.execute(db, query, [id]).then(function(res) {
          console.log("deleted : " + id + " " + res);
        }, function (err) {
          console.error("Error Delete Category");
          console.error( err);
        });
      },

      update: function(id,name){
        var query = "UPDATE sub_category set name = ? where  id = ?";
        $cordovaSQLite.execute(db, query, [name,id]).then(function(res) {
          console.log("deleted : " + id + " " + res);
        }, function (err) {
          console.error("Error Delete Category");
          console.error( err);
        });
      },

      add: function(name,catid) {
        var query = "INSERT INTO sub_category(catid, name) VALUES (?,?)";
        $cordovaSQLite.execute(db, query, [catid, name]).then(function(res) {
          console.log("insertId: " + res);
        }, function (err) {
          console.error("Error Add SubCategory");
          console.error( err);
        });
      }

    };
  })

.factory('Data', function($cordovaSQLite){

	return{
		all: function(){
			var query = "select * from data";
			console.log(query);
			$cordovaSQLite.execute(db, query, []).then(function( res) {
				console.log(res.rows);
				console.log(res.rows.item(0));
			//	return res.rows;
			}, function (err) {
				console.error( "all - Search Error" + err);
			//	return -1;
			});
		},

    // show all te data from category id;
		allType: function(catid,subcatid){
			var query = "select * from data where catid = ? AND subcatid = ?";
      console.log("catID: "+ catid + " sub: " +subcatid );

      var catData= [];
			$cordovaSQLite.execute(db, query, [catid,subcatid]).then(function( res) {
				console.log(res.rows);
				console.log(res.rows.item(0));
        for(var i=0 ;i < res.rows.length ; i++ ){
          catData.push(res.rows.item(i));
        }
			//	return res.rows;
			}, function (err) {
				console.error( "all - Search Error" + err);
			//	return -1;
			});
      return catData;
		},

    getData: function(catid,subcatid,dataid){
      var query = "select * from data where catid = ? AND subcatid = ? AND id = ?";
      console.log("ID: "+ dataid+"catID: "+ catid + " sub: " +subcatid );

      var data= [];
      $cordovaSQLite.execute(db, query, [catid,subcatid,dataid]).then(function( res) {
        console.log(res.rows);
        console.log(res.rows.item(0));
        for(var i=0 ;i < res.rows.length ; i++ ){
          data.push(res.rows.item(i));
        }
        //	return res.rows;
      }, function (err) {
        console.error( "all - Search Error" + err);
        //	return -1;
      });
      return data;
    },


    delete: function(id){
      var query = "DELETE FROM data where  id = ?";
      $cordovaSQLite.execute(db, query, [id]).then(function(res) {
        console.log("deleted : " + id + " " + res);
      }, function (err) {
        console.error("Error Delete Category");
        console.error( err);
      });
    },

    //TODO:: update query
   /* update: function(id,name){
      var query = "UPDATE data set name = ? where  id = ?";
      $cordovaSQLite.execute(db, query, [name,id]).then(function(res) {
        console.log("deleted : " + id + " " + res);
      }, function (err) {
        console.error("Error Delete Category");
        console.error( err);
      });
    },
*/
		add: function(catid, subcatid, name, date, paid,comment) {
			alert(" Name: "+name);
				var query = "INSERT INTO data(catid, subcatid, name, date, paid, comment) VALUES (?,?,?,?,?,?)";
				$cordovaSQLite.execute(db, query, [catid, subcatid, name, date, paid, comment]).then(function(res) {
				console.log("insertId: " + res);
			  }, function (err) {
				console.error("Error Add Data");
				console.error( err);
				});
			}

	};
});

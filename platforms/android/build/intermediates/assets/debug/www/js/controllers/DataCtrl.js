/**
 * Created by Nassar on 3/12/2016.
 */
app.controller('DataCtrl', function($scope, $stateParams , Data, $ionicModal, $ionicPopup) {
  $scope.cat = {};
  var catId = $stateParams.catId;
  var subCatId = $stateParams.subCatId;
  console.log($stateParams);
  $scope.shouldShowDelete = false;
  $scope.shouldShowReorder = false;
  $scope.listCanSwipe = true ;

  var chats = [{  //TODO: Delete
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];
  $scope.data = chats;  //TODO: Delete

  var init = function(){
    $scope.data = Data.allType(catId,subCatId);
  };

  $ionicModal.fromTemplateUrl('templates/modals/addDataModal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.addData = modal;
  });


  $ionicModal.fromTemplateUrl('templates/modals/editDataModal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.editData = modal;
  });

  $scope.edit = function(dataId){
    dataId = dataId;
    $scope.editData.show();
  };


  $scope.createData = function(cat) {
    addcat.$submitted = true;
    Data.add(catId, subCatId, cat.name, cat.date, cat.paid, cat.comment);
    $scope.addData.hide();
    $scope.data = Data.allType(catId,subCatId);
  };
//TODO: call for update data service when created
  $scope.updateData = function(cat) {
    editcat.$submitted = true;
    // Category.update(dataId,cat.name);
    $scope.editData.hide();
    $scope.data = Data.allType(catId,subCatId);
  };

  $ionicModal.fromTemplateUrl('templates/modals/viewDataModal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.viewData = modal;
  });

  $scope.view = function(dataId){
    $scope.viewObjData = Data.getData(catId,subCatId,dataId);
    $scope.viewData.show();
  };
  $scope.viewData = function(dataId) {
    // Category.update(dataId,cat.name);
    $scope.viewData.hide();

    $scope.view = Data.allType(catId,subCatId);
  };

  $scope.deleteConfirm = function(id,name) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Delete Category',
      template: 'Are you sure you want to Delete "' + name + '" Category?'
    });

    confirmPopup.then(function (res) {
      if (res) {
        console.log("Confirmed")
        Data.delete(id);
        $scope.data = Data.allType(catId,subCatId);
      }
    });
  }
  //TODO: uncomment when using the database
  //  init();
});

/**
 * Created by Nassar on 3/12/2016.
 */

app.controller('SubCatCtrl', function($rootScope, $scope, $cordovaSQLite, SubCategory, Data , $ionicModal, $ionicPopup, $stateParams) {

  $scope.shouldShowDelete = false;
  $scope.shouldShowReorder = false;
  $scope.listCanSwipe = true ;
  var categoryId = -1;
  var catId = $stateParams.catId;
  $scope.catId = $stateParams.catId;
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  $scope.category = {};
  $scope.category = chats;

  $scope.test = function(){

    //Category.add('eassa');
    //	Category.add('nassar');
    $scope.category =  SubCategory.all(catId);
    //	alert(JSON.stringify(Category.all()));
    //	Data.add(1,new Date(), 100.21);
    //	Data.all();
    //	Data.allType(1);

  };

  $ionicModal.fromTemplateUrl('templates/modals/addCategoryModal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.addCat = modal;
  });

  $ionicModal.fromTemplateUrl('templates/modals/editCategoryModal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.editCat = modal;
  });

  $scope.edit = function(catId){
    categoryId = catId;
    $scope.editCat.show();
  };


  $scope.createCategory = function(cat) {
    addcat.$submitted = true;
    SubCategory.add(cat.name,catId);
    $scope.addCat.hide();
    $scope.category =  SubCategory.all(catId);
  };

  $scope.editCategory = function(cat) {
    editcat.$submitted = true;
    SubCategory.update(categoryId,cat.name);
    $scope.editCat.hide();
    $scope.category =  SubCategory.all(catId);
  };

  $scope.deleteConfirm = function(id,name) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Delete "'+ name+ '"',
      template: 'Are you sure you want to Delete "'+ name+ '" Category?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        console.log("Confirmed")
        SubCategory.delete(id);
        $scope.category =  SubCategory.all(catId);
      }
    });
  };


  var init = function(){
    $scope.test();
  };

  $scope.deferred.promise.then(function() {
    init();
  });


})

app.controller('ChatsCtrl', function($scope, $cordovaSQLite ,Category,Data) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
});


app.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});

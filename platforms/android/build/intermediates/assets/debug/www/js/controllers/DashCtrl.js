/**
 * Created by Nassar on 3/12/2016.
 */
var app = angular.module('starter.controllers', []);
app.controller('DashCtrl', function($rootScope, $scope, $cordovaSQLite, Category, Data , $ionicModal, $ionicPopup) {
  /*
   $scope.insert = function(firstname, lastname) {
   var db = $cordovaSQLite.openDB({ name: "my.db" });

   // for opening a background db:
   var db = $cordovaSQLite.openDB({ name: "my.db", bgType: 1 });

   $scope.execute = function() {
   var query = "INSERT INTO test_table (data, data_num) VALUES (?,?)";
   $cordovaSQLite.execute(db, query, ["test", 100]).then(function(res) {
   console.log("insertId: " + res.insertId);
   }, function (err) {
   console.error(err);
   });
   };
   */
  $scope.shouldShowDelete = false;
  $scope.shouldShowReorder = false;
  $scope.listCanSwipe = true ;
  var categoryId = -1;

  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  $scope.category = {};
  $scope.category = chats;

  $scope.test = function(){

    //Category.add('eassa');
    //	Category.add('nassar');
    $scope.category =  Category.all();
    //	alert(JSON.stringify(Category.all()));
    //	Data.add(1,new Date(), 100.21);
    //	Data.all();
    //	Data.allType(1);

  };

  $ionicModal.fromTemplateUrl('templates/modals/addCategoryModal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.addCat = modal;
  });

  $ionicModal.fromTemplateUrl('templates/modals/editCategoryModal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.editCat = modal;
  });

  $scope.edit = function(catId){
    categoryId = catId;
    $scope.editCat.show();
  };


  $scope.createCategory = function(cat) {
    addcat.$submitted = true;
    Category.add(cat.name);
    $scope.addCat.hide();
    $scope.category =  Category.all();
  };

  $scope.editCategory = function(cat) {
    editcat.$submitted = true;
    Category.update(categoryId,cat.name);
    $scope.editCat.hide();
    $scope.category =  Category.all();
  };

  $scope.deleteConfirm = function(id,name) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Delete Category',
      template: 'Are you sure you want to Delete "'+ name+ '" Category?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        console.log("Confirmed")
        Category.delete(id);
        $scope.category =  Category.all();
      }
    });
  };


  var init = function(){
    $scope.test();
  };

  $scope.deferred.promise.then(function() {
    init();
  });

});
